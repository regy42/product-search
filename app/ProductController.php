<?php

class ProductController
{
    //TODO: extract access counter to a separate class

    const ACCESS_COUNTER_KEY = "accessCounter";

    /** @var  IElasticSearchDriver */
    private $elasticDriver;

    /** @var  IMySQLDriver */
    private $sqlDriver;

    /** @var  \Nette\Caching\IStorage */
    private $cacheStorage;

    /** @var  \Nette\Caching\Cache */
    private $cache;

    /** @var  array */
    private $accessCounter;

    /** @var  boolean */
    private $useElastic;

    /**
     * ProductController constructor.
     * @param boolean $useElastic
     * @param IElasticSearchDriver $elasticDriver
     * @param IMySQLDriver $sqlDriver
     * @param \Nette\Caching\IStorage $cacheStorage
     */
    public function __construct( $useElastic, IElasticSearchDriver $elasticDriver, IMySQLDriver $sqlDriver, \Nette\Caching\IStorage $cacheStorage)
    {
        $this->useElastic = $useElastic;
        $this->elasticDriver = $elasticDriver;
        $this->sqlDriver = $sqlDriver;
        $this->cacheStorage = $cacheStorage;
        $this->cache = new \Nette\Caching\Cache($cacheStorage);

        // init access counter;
        $this->accessCounter = $this->cache->load(ProductController::ACCESS_COUNTER_KEY);
        if ($this->accessCounter === null)
            $this->accessCounter = [];

    }


    /**
     * @param string $id
     * @return string
     */
    public function detail($id)
    {

        $product = $this->cache->load($id);

        if($product === null) {
            $product = $this->useElastic? $this->elasticDriver->findById($id) : $this->sqlDriver->findProduct($id);
            $this->cache->save($id,$product);
        }

        $this->increaseAccessCount($id);

        return json_encode($product);
    }

    protected function increaseAccessCount($id) {
        if(isset($this->accessCounter[$id]))
            $this->accessCounter[$id]++;
        else
            $this->accessCounter[$id] = 1;

        $this->cache->save(ProductController::ACCESS_COUNTER_KEY,$this->accessCounter);

    }

    public function getAccessStats() {
        return $this->accessCounter;
    }
}

