<?php
/**
 * Created by IntelliJ IDEA.
 * User: Acer
 * Date: 19.08.2018
 * Time: 17:09
 */
interface IElasticSearchDriver
{
    /**
     * @param string $id
     * @return array
     */
    public function findById($id);
}