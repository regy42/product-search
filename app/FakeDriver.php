<?php


class FakeDriver implements IMySQLDriver, IElasticSearchDriver {


    /**
     * @param string $id
     * @return array
     */
    public function findProduct($id)
    {
        if($id == 1)
            return [
                "title" => "product 1",
                "desc" => "lorem ipsum"
            ];

        if($id == 2)
            return [
                "title" => "product 2",
                "desc" => "lorem ipsum"
            ];

        if($id == 3)
            return [
                "title" => "product 3",
                "desc" => "lorem ipsum"
            ];


        return [
            "title" => "error",
            "desc" => "product not found"
        ];
    }

    /**
     * @param string $id
     * @return array
     */
    public function findById($id)
    {
        return $this->findProduct($id);
    }

}