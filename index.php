<?php

require __DIR__ . '/vendor/autoload.php';

$loader = new Nette\Loaders\RobotLoader;

// Add directories for RobotLoader to index
$loader->addDirectory(__DIR__ . '/app');

// And set caching to the 'temp' directory
$loader->setTempDirectory(__DIR__ . '/temp');
$loader->register(); // Run the RobotLoader

$controller = new ProductController(false,new FakeDriver(),new FakeDriver(),new \Nette\Caching\Storages\FileStorage("temp"));

echo $controller->detail(1);

print_r($controller->getAccessStats());



